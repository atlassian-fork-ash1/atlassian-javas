class javas::params {
  $main_distribution = 'java8'
  $default_platform = 'linux-x64'
  $distributions = {
    'java8' => {
      'version' => '8',
      'update'  => '121',
      'build'   => 'b13',
      'type'    => 'jdk',
      'hash'    => 'e9e7ea248e2c4826b92b3f075a80e441',
    },
  }
  $download_url = 'http://download.oracle.com/otn-pub/java/jdk/{version}u{update}-{build}/{hash}/{type}-{version}u{update}-{platform}.tar.gz'
  $distributions_dir = '/java'
  $jce_urls = {
    '6' => 'http://download.oracle.com/otn-pub/java/jce_policy/6/jce_policy-6.zip',
    '7' => 'http://download.oracle.com/otn-pub/java/jce/7/UnlimitedJCEPolicyJDK7.zip',
    '8' => 'http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip',
  }
}
